import Vue from 'vue'
import App from './App.vue'
import * as Sentry from '@sentry/vue';

Sentry.init({
  Vue: Vue,
  dsn: '__PUBLIC_DSN__',
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
